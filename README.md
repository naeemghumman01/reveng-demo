# README #

### Description ###
Integration of [DB Reverse Engineering Plugin](https://grails-plugins.github.io/grails-db-reverse-engineer/grails3v4/index.html) with **`GRAILS 3.0.X`**

### Required Tools ###

* `Grails-3.0.17`
* `Java-1.8.x`
* `MySql-8.0`

> Note: `JAVA and GRAILS` must be on class path to run this project.

### Update Settings ###

* Change database name in application.groovy file
  > `reveng-demo > grails-app > conf > application.groovy`

* Update database credentials `(username and password)` and change the database name`environments > development > datasource > url`) in application.yml file  
  > `reveng-demo > grails-app > conf > application.yml`
  
### Command for DB Reverse Engineering ###
> From project root directory execute the below [command](https://grails-plugins.github.io/grails-db-reverse-engineer/grails3v4/index.html#run-the-db-reverse-engineer-command) 
> based on your Operating System. The plugin will generate domain classes under `grails-app > domain`  
 
* Windows `gradlew dbReverseEngineer`
* Linux `./gradlew dbReverseEngineer`

## User Guide ##
[User Guide](https://grails-plugins.github.io/grails-db-reverse-engineer/grails3v4/index.html)


